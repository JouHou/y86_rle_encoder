Y86 RLE encoder

A small run-length encoder/packer written in Y86 assembly language. Course assignment for University of Oulu Computer Systems course.

- The saved space is given as percents in the rax-register.

Simulator can be found in here: http://csapp.cs.cmu.edu/3e/students.html (chapter 4)